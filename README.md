# postman-ci-pipeline-example


[![Build Status](https://img.shields.io/gitlab/pipeline/DannyDainton/postman-ci-pipeline-example.svg)](https://gitlab.com/DannyDainton/postman-ci-pipeline-example)

---

This is small example project to show a Postman Collection running with the `Newman` Docker Image inside the Gitlab CI environment.


## GitLab

### How does it work with GitLab

All the magic happens in the `.gitlab-ci.yml` file. This is using the `postman/newman_alpine33` Docker image, to run the collection file.

```yml
stages:
    - test

newman_tests:
    stage: test
    image:
        name: postman/newman_alpine33
        entrypoint: [""]
    script:
        - newman --version
        - newman run ./Restful_Booker_Collection.postman_collection.json -e ./Restful_Booker_Environment.postman_environment.json
```

As this is a basic example of 'it working', it's just logging the results out to the console. You'll be able to see this buy selecting `CI / CD` > `Jobs` from the left side menu. 

You will also be able to use the reporters to create things like HTML or JSON reports of the run by extending the newman command in the `script:` section.

```yml
stages:
    - test

newman_tests:
    stage: test
    image:
        name: postman/newman_alpine33
        entrypoint: [""]
    script:
        - newman --version
        - newman run ./Restful_Booker_Collection.postman_collection.json -e ./Restful_Booker_Environment.postman_environment.json --reporters cli,htmlextra --reporter-htmlextra-export testReport.html
    artifacts:
        when: always
        paths:
            - testReport.html

```

By added the `arifacts:` section, this will save the report and can be downloaded from Gitlab.

If you have any questions, you can drop me a message on Twitter `@dannydainton`.


